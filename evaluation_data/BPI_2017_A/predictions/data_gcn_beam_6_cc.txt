FULL
{
  "fitness": "0.886",
  "fscore": "0.939",
  "generalization": "0.991",
  "metricsAverageWeight": "0.914",
  "precision": "1.000",
  "simplicity": "0.778"
}
Top 30:
{
  "fitness": "0.890",
  "fscore": "0.918",
  "generalization": "0.991",
  "metricsAverageWeight": "0.902",
  "precision": "0.949",
  "simplicity": "0.778"
}