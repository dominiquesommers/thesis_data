FULL
{
  "fitness": "0.937",
  "fscore": "0.967",
  "generalization": "0.992",
  "metricsAverageWeight": "0.927",
  "precision": "1.000",
  "simplicity": "0.778"
}
Top 30:
{
  "fitness": "0.936",
  "fscore": "0.935",
  "generalization": "0.992",
  "metricsAverageWeight": "0.910",
  "precision": "0.935",
  "simplicity": "0.778"
}