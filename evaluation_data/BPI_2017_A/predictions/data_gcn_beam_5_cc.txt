FULL
{
  "fitness": "0.912",
  "fscore": "0.954",
  "generalization": "0.992",
  "metricsAverageWeight": "0.920",
  "precision": "1.000",
  "simplicity": "0.778"
}
Top 30:
{
  "fitness": "0.911",
  "fscore": "0.921",
  "generalization": "0.992",
  "metricsAverageWeight": "0.903",
  "precision": "0.932",
  "simplicity": "0.778"
}