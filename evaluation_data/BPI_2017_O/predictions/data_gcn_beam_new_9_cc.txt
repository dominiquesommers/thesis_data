{
  "easy_soundness": true,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2017_O/predictions/data_gcn_beam_new_9_cc.txt",
  "full": {
    "fitness                         ": "0.93116",
    "fitness_alignments              ": "0.91988",
    "fitness_alignments_percFitTraces": "52.17874",
    "fscore                          ": "0.96263",
    "generalization                  ": "0.98820",
    "metricsAverageWeight            ": "0.89997",
    "precision                       ": "0.99630",
    "precision_alignments            ": "0.99630",
    "simplicity                      ": "0.68421"
  },
  "s_components": [
    [
      "p1",
      "O_Create Offer",
      "p2",
      "p3",
      "O_Returned",
      "sink",
      "O_Sent (mail and online)",
      "p7",
      "st0",
      "|",
      "source",
      "O_Sent (online only)",
      "p5",
      "st2",
      "p0",
      "p4",
      "short_circuited_transition",
      "p6",
      "O_Cancelled",
      "O_Created",
      "st3",
      "O_Refused",
      "st1",
      "st5",
      ">",
      "O_Accepted",
      "st4"
    ]
  ],
  "sound": true,
  "top 30": {
    "fitness                         ": "0.93116",
    "fitness_alignments              ": "0.91988",
    "fitness_alignments_percFitTraces": "52.17874",
    "fscore                          ": "0.96263",
    "generalization                  ": "0.98820",
    "metricsAverageWeight            ": "0.89997",
    "precision                       ": "0.99630",
    "precision_alignments            ": "0.99630",
    "simplicity                      ": "0.68421"
  },
  "uncovered_places_s_component": []
}