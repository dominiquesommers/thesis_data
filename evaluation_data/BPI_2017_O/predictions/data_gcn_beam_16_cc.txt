FULL
{
  "fitness": "0.915",
  "fscore": "0.954",
  "generalization": "0.987",
  "metricsAverageWeight": "0.886",
  "precision": "0.996",
  "simplicity": "0.647"
}
Top 30:
{
  "fitness": "0.931",
  "fscore": "0.935",
  "generalization": "0.983",
  "metricsAverageWeight": "0.875",
  "precision": "0.939",
  "simplicity": "0.647"
}