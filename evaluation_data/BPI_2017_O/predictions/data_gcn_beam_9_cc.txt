FULL
{
  "fitness": "0.882",
  "fscore": "0.882",
  "generalization": "0.985",
  "metricsAverageWeight": "0.849",
  "precision": "0.883",
  "simplicity": "0.647"
}
Top 30:
{
  "fitness": "0.887",
  "fscore": "0.863",
  "generalization": "0.913",
  "metricsAverageWeight": "0.822",
  "precision": "0.840",
  "simplicity": "0.647"
}