{
  "easy_soundness": true,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2017_O/predictions/data_gcn_beam_new_18_cc.txt",
  "full": {
    "fitness                         ": "0.90376",
    "fitness_alignments              ": "0.67334",
    "fitness_alignments_percFitTraces": "0.57444",
    "fscore                          ": "0.94070",
    "generalization                  ": "0.98710",
    "metricsAverageWeight            ": "0.88041",
    "precision                       ": "0.98080",
    "precision_alignments            ": "0.98080",
    "simplicity                      ": "0.65000"
  },
  "s_components": [],
  "sound": false,
  "top 30": {
    "fitness                         ": "0.90376",
    "fitness_alignments              ": "0.67334",
    "fitness_alignments_percFitTraces": "0.57444",
    "fscore                          ": "0.94070",
    "generalization                  ": "0.98710",
    "metricsAverageWeight            ": "0.88041",
    "precision                       ": "0.98080",
    "precision_alignments            ": "0.98080",
    "simplicity                      ": "0.65000"
  },
  "uncovered_places_s_component": [
    "p6",
    "source",
    "p0",
    "p5",
    "p2",
    "p3",
    "sink",
    "p7",
    "p1",
    "p4"
  ]
}