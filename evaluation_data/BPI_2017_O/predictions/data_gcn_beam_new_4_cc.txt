{
  "easy_soundness": true,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2017_O/predictions/data_gcn_beam_new_4_cc.txt",
  "full": {
    "fitness                         ": "0.88464",
    "fitness_alignments              ": "0.87444",
    "fitness_alignments_percFitTraces": "10.98734",
    "fscore                          ": "0.93688",
    "generalization                  ": "0.98675",
    "metricsAverageWeight            ": "0.88782",
    "precision                       ": "0.99568",
    "precision_alignments            ": "0.99568",
    "simplicity                      ": "0.68421"
  },
  "s_components": [
    [
      "short_circuited_transition",
      "st3",
      "O_Accepted",
      "st0",
      "p3",
      "p0",
      "p2",
      "st5",
      "p6",
      "sink",
      "|",
      "source",
      "O_Refused",
      "O_Created",
      "O_Cancelled",
      "p1",
      "O_Returned",
      "O_Sent (mail and online)",
      "p5",
      "O_Create Offer",
      "st4",
      "p7",
      "st1",
      "st2",
      ">",
      "O_Sent (online only)",
      "p4"
    ]
  ],
  "sound": true,
  "top 30": {
    "fitness                         ": "0.88464",
    "fitness_alignments              ": "0.87444",
    "fitness_alignments_percFitTraces": "10.98734",
    "fscore                          ": "0.93688",
    "generalization                  ": "0.98675",
    "metricsAverageWeight            ": "0.88782",
    "precision                       ": "0.99568",
    "precision_alignments            ": "0.99568",
    "simplicity                      ": "0.68421"
  },
  "uncovered_places_s_component": []
}