FULL
{
  "fitness": "0.797",
  "fscore": "0.885",
  "generalization": "0.985",
  "metricsAverageWeight": "0.864",
  "precision": "0.996",
  "simplicity": "0.677"
}
Top 30:
{
  "fitness": "0.787",
  "fscore": "0.860",
  "generalization": "0.976",
  "metricsAverageWeight": "0.847",
  "precision": "0.948",
  "simplicity": "0.677"
}