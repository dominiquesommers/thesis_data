FULL
{
  "fitness": "0.847",
  "fscore": "0.916",
  "generalization": "0.986",
  "metricsAverageWeight": "0.877",
  "precision": "0.996",
  "simplicity": "0.677"
}
Top 30:
{
  "fitness": "0.853",
  "fscore": "0.897",
  "generalization": "0.978",
  "metricsAverageWeight": "0.864",
  "precision": "0.947",
  "simplicity": "0.677"
}