FULL
{
  "fitness": "0.661",
  "fscore": "0.738",
  "generalization": "0.859",
  "metricsAverageWeight": "0.736",
  "precision": "0.835",
  "simplicity": "0.586"
}
Top 30:
{
  "fitness": "0.647",
  "fscore": "0.481",
  "generalization": "0.770",
  "metricsAverageWeight": "0.596",
  "precision": "0.383",
  "simplicity": "0.586"
}