FULL
{
  "fitness": "0.704",
  "fscore": "0.703",
  "generalization": "0.902",
  "metricsAverageWeight": "0.699",
  "precision": "0.702",
  "simplicity": "0.489"
}
Top 30:
{
  "fitness": "0.702",
  "fscore": "0.488",
  "generalization": "0.768",
  "metricsAverageWeight": "0.583",
  "precision": "0.374",
  "simplicity": "0.489"
}