FULL
{
  "fitness": "0.661",
  "fscore": "0.732",
  "generalization": "0.860",
  "metricsAverageWeight": "0.739",
  "precision": "0.821",
  "simplicity": "0.614"
}
Top 30:
{
  "fitness": "0.642",
  "fscore": "0.527",
  "generalization": "0.774",
  "metricsAverageWeight": "0.620",
  "precision": "0.448",
  "simplicity": "0.614"
}