FULL
{
  "fitness": "0.730",
  "fscore": "0.399",
  "generalization": "0.831",
  "metricsAverageWeight": "0.582",
  "precision": "0.274",
  "simplicity": "0.491"
}
Top 30:
{
  "fitness": "0.734",
  "fscore": "0.351",
  "generalization": "0.801",
  "metricsAverageWeight": "0.564",
  "precision": "0.230",
  "simplicity": "0.491"
}