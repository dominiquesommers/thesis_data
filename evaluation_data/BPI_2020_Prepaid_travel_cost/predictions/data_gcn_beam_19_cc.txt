FULL
{
  "fitness": "0.746",
  "fscore": "0.401",
  "generalization": "0.806",
  "metricsAverageWeight": "0.580",
  "precision": "0.274",
  "simplicity": "0.491"
}
Top 30:
{
  "fitness": "0.751",
  "fscore": "0.353",
  "generalization": "0.776",
  "metricsAverageWeight": "0.562",
  "precision": "0.230",
  "simplicity": "0.491"
}