FULL
{
  "fitness": "0.594",
  "fscore": "0.745",
  "generalization": "0.700",
  "metricsAverageWeight": "0.670",
  "precision": "1.000",
  "simplicity": "0.385"
}
Top 30:
{
  "fitness": "0.595",
  "fscore": "0.746",
  "generalization": "0.680",
  "metricsAverageWeight": "0.665",
  "precision": "1.000",
  "simplicity": "0.385"
}