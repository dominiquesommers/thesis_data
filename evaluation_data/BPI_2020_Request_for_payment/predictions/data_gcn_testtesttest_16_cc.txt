{
  "easy_soundness": true,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2020_Request_for_payment/predictions/data_gcn_testtesttest_16_cc.txt",
  "full": {
    "fitness                         ": "0.89722",
    "fitness_alignments              ": "0.83801",
    "fitness_alignments_percFitTraces": "25.58815",
    "fscore                          ": "0.90948",
    "fscore_alignments               ": "0.91187",
    "generalization                  ": "0.98628",
    "metricsAverageWeight            ": "0.95140",
    "precision                       ": "0.92209",
    "precision_alignments            ": "1.00000",
    "simplicity                      ": "1.00000"
  },
  "s_components": [
    [
      "p5",
      "p3",
      "sink",
      "source",
      "Request Payment",
      "Request For Payment SUBMITTED by EMPLOYEE",
      "|",
      ">",
      "Payment Handled",
      "p2",
      "p6",
      "p0",
      "Request For Payment FINAL_APPROVED by SUPERVISOR",
      "short_circuited_transition"
    ],
    [
      "p5",
      "p3",
      "sink",
      "p4",
      "source",
      "Request Payment",
      "Request For Payment APPROVED by ADMINISTRATION",
      "Request For Payment APPROVED by BUDGET OWNER",
      "p7",
      "Request For Payment SUBMITTED by EMPLOYEE",
      "|",
      ">",
      "Payment Handled",
      "p1",
      "p6",
      "p0",
      "Request For Payment FINAL_APPROVED by SUPERVISOR",
      "short_circuited_transition"
    ]
  ],
  "sound": true,
  "top 30": {
    "fitness                         ": "0.90020",
    "fitness_alignments              ": "0.84233",
    "fitness_alignments_percFitTraces": "25.94229",
    "fscore                          ": "0.91287",
    "fscore_alignments               ": "0.91442",
    "generalization                  ": "0.98617",
    "metricsAverageWeight            ": "0.95307",
    "precision                       ": "0.92591",
    "precision_alignments            ": "1.00000",
    "simplicity                      ": "1.00000"
  },
  "uncovered_places_s_component": []
}