FULL
{
  "fitness": "0.900",
  "fscore": "0.559",
  "generalization": "0.811",
  "metricsAverageWeight": "0.653",
  "precision": "0.405",
  "simplicity": "0.495"
}
Top 30:
{
  "fitness": "0.902",
  "fscore": "0.531",
  "generalization": "0.807",
  "metricsAverageWeight": "0.645",
  "precision": "0.377",
  "simplicity": "0.495"
}