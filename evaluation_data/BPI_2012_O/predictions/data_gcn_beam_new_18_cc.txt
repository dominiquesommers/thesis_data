{
  "easy_soundness": true,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2012_O/predictions/data_gcn_beam_new_18_cc.txt",
  "full": {
    "fitness                         ": "0.80570",
    "fitness_alignments              ": "0.81223",
    "fitness_alignments_percFitTraces": "30.64806",
    "fscore                          ": "0.89152",
    "generalization                  ": "0.82686",
    "metricsAverageWeight            ": "0.82426",
    "precision                       ": "0.99780",
    "precision_alignments            ": "0.99780",
    "simplicity                      ": "0.66667"
  },
  "s_components": [],
  "sound": false,
  "top 30": {
    "fitness                         ": "0.80591",
    "fitness_alignments              ": "0.83264",
    "fitness_alignments_percFitTraces": "32.57046",
    "fscore                          ": "0.89192",
    "generalization                  ": "0.82523",
    "metricsAverageWeight            ": "0.82407",
    "precision                       ": "0.99848",
    "precision_alignments            ": "0.99848",
    "simplicity                      ": "0.66667"
  },
  "uncovered_places_s_component": [
    "p6",
    "p1",
    "p5",
    "p0",
    "p3",
    "source",
    "p7",
    "p4",
    "p2",
    "sink",
    "p8"
  ]
}