{
  "easy_soundness": false,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2012_O/predictions/data_gcn_beam_new_8_cc.txt",
  "full": {
    "fitness                         ": "0.84959",
    "fscore                          ": "0.76296",
    "generalization                  ": "0.89541",
    "metricsAverageWeight            ": "0.78122",
    "precision                       ": "0.69236",
    "simplicity                      ": "0.68750"
  },
  "s_components": [],
  "sound": false,
  "top 30": {
    "fitness                         ": "0.84968",
    "fscore                          ": "0.76420",
    "generalization                  ": "0.89127",
    "metricsAverageWeight            ": "0.78070",
    "precision                       ": "0.69434",
    "simplicity                      ": "0.68750"
  },
  "uncovered_places_s_component": [
    "p3",
    "p6",
    "p0",
    "p4",
    "sink",
    "p2",
    "p1",
    "source",
    "p5"
  ]
}