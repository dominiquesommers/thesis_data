{
  "easy_soundness": true,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2012_O/predictions/data_gcn_beam_new_7_cc.txt",
  "full": {
    "fitness                         ": "0.94987",
    "fitness_alignments              ": "0.88885",
    "fitness_alignments_percFitTraces": "71.32602",
    "fscore                          ": "0.89873",
    "generalization                  ": "0.98213",
    "metricsAverageWeight            ": "0.87953",
    "precision                       ": "0.85281",
    "precision_alignments            ": "0.85281",
    "simplicity                      ": "0.73333"
  },
  "s_components": [
    [
      "p2",
      "sink",
      "p1",
      ">",
      "p6",
      "st1",
      "p3",
      "source",
      "p0",
      "short_circuited_transition",
      "O_ACCEPTED",
      "O_CREATED",
      "O_SENT",
      "O_DECLINED",
      "O_SELECTED",
      "|"
    ],
    [
      "p2",
      "p4",
      "p7",
      "sink",
      "p1",
      ">",
      "p0",
      "source",
      "st0",
      "O_SENT_BACK",
      "short_circuited_transition",
      "O_CREATED",
      "st2",
      "O_SENT",
      "p5",
      "O_SELECTED",
      "|",
      "O_CANCELLED"
    ]
  ],
  "sound": true,
  "top 30": {
    "fitness                         ": "0.96312",
    "fitness_alignments              ": "0.91349",
    "fitness_alignments_percFitTraces": "75.79996",
    "fscore                          ": "0.90757",
    "generalization                  ": "0.98120",
    "metricsAverageWeight            ": "0.88393",
    "precision                       ": "0.85807",
    "precision_alignments            ": "0.85807",
    "simplicity                      ": "0.73333"
  },
  "uncovered_places_s_component": []
}