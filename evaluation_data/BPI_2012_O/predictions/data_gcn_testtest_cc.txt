FULL
{
  "fitness": "0.759",
  "fscore": "0.863",
  "generalization": "0.982",
  "metricsAverageWeight": "0.858",
  "precision": "1.000",
  "simplicity": "0.692"
}
Top 30:
{
  "fitness": "0.761",
  "fscore": "0.864",
  "generalization": "0.981",
  "metricsAverageWeight": "0.859",
  "precision": "1.000",
  "simplicity": "0.692"
}