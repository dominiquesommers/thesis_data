{
  "easy_soundness": false,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2012_O/predictions/data_gcn_beam_new_11_cc.txt",
  "full": {
    "fitness                         ": "0.86271",
    "fscore                          ": "0.76640",
    "generalization                  ": "0.96936",
    "metricsAverageWeight            ": "0.80225",
    "precision                       ": "0.68942",
    "simplicity                      ": "0.68750"
  },
  "s_components": [],
  "sound": false,
  "top 30": {
    "fitness                         ": "0.85442",
    "fscore                          ": "0.75886",
    "generalization                  ": "0.96436",
    "metricsAverageWeight            ": "0.79720",
    "precision                       ": "0.68253",
    "simplicity                      ": "0.68750"
  },
  "uncovered_places_s_component": [
    "p1",
    "p5",
    "p2",
    "p4",
    "p3",
    "sink",
    "p0",
    "source",
    "p6"
  ]
}