FULL
{
  "fitness": "0.729",
  "fscore": "0.710",
  "generalization": "0.982",
  "metricsAverageWeight": "0.768",
  "precision": "0.693",
  "simplicity": "0.667"
}
Top 30:
{
  "fitness": "0.741",
  "fscore": "0.716",
  "generalization": "0.981",
  "metricsAverageWeight": "0.770",
  "precision": "0.694",
  "simplicity": "0.667"
}