{
  "easy_soundness": false,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2012_O/predictions/data_gcn_beam_new_12_cc.txt",
  "full": {
    "fitness                         ": "0.85633",
    "fscore                          ": "0.86953",
    "generalization                  ": "0.89433",
    "metricsAverageWeight            ": "0.83948",
    "precision                       ": "0.88313",
    "simplicity                      ": "0.72414"
  },
  "s_components": [],
  "sound": false,
  "top 30": {
    "fitness                         ": "0.85685",
    "fscore                          ": "0.87018",
    "generalization                  ": "0.89215",
    "metricsAverageWeight            ": "0.83927",
    "precision                       ": "0.88393",
    "simplicity                      ": "0.72414"
  },
  "uncovered_places_s_component": [
    "p0",
    "sink",
    "p5",
    "p6",
    "p2",
    "p1",
    "source",
    "p4",
    "p3"
  ]
}