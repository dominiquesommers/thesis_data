{
  "easy_soundness": true,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2020_Domestic_declarations/predictions/data_heuristics_reduced_cc.txt",
  "full": {
    "fitness                         ": "0.93677",
    "fitness_alignments              ": "0.88532",
    "fitness_alignments_percFitTraces": "35.37143",
    "fscore                          ": "0.90055",
    "fscore_alignments               ": "0.87608",
    "generalization                  ": "0.76997",
    "metricsAverageWeight            ": "0.80031",
    "precision                       ": "0.86704",
    "precision_alignments            ": "0.86704",
    "simplicity                      ": "0.62745"
  },
  "s_components": [],
  "sound": false,
  "uncovered_places_s_component": [
    "source",
    "n3",
    "n10",
    "n1",
    "n17",
    "n12",
    "n6",
    "n15",
    "n4",
    "n16",
    "n19",
    "n7",
    "n8",
    "n20",
    "n22",
    "n13",
    "n23",
    "n9",
    "n18",
    "n21",
    "n14",
    "n2",
    "n11",
    "n5"
  ]
}