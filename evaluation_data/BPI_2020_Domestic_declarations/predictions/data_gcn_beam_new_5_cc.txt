{
  "easy_soundness": false,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2020_Domestic_declarations/predictions/data_gcn_beam_new_5_cc.txt",
  "full": {
    "fitness                         ": "0.77839",
    "fscore                          ": "0.49359",
    "generalization                  ": "0.77634",
    "metricsAverageWeight            ": "0.58664",
    "precision                       ": "0.36137",
    "simplicity                      ": "0.43046"
  },
  "s_components": [],
  "sound": false,
  "top 30": {
    "fitness                         ": "0.77881",
    "fscore                          ": "0.41417",
    "generalization                  ": "0.76282",
    "metricsAverageWeight            ": "0.56355",
    "precision                       ": "0.28210",
    "simplicity                      ": "0.43046"
  },
  "uncovered_places_s_component": [
    "p18",
    "source",
    "p2",
    "p14",
    "p4",
    "p5",
    "p10",
    "p21",
    "p0",
    "p17",
    "p11",
    "p1",
    "p16",
    "p3",
    "p6",
    "p20",
    "p22",
    "sink",
    "p7",
    "p13",
    "p19",
    "p12",
    "p15",
    "p9",
    "p8"
  ]
}