{
  "easy_soundness": false,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2020_Domestic_declarations/predictions/data_gcn_candidates_frequency_new_8_traces_testtesttest_3_cc.txt",
  "full": {
    "fitness                         ": "0.87389",
    "fscore                          ": "0.92811",
    "generalization                  ": "0.97608",
    "metricsAverageWeight            ": "0.89926",
    "precision                       ": "0.98950",
    "simplicity                      ": "0.75758"
  },
  "s_components": [],
  "sound": false,
  "top 30": {
    "fitness                         ": "0.87523",
    "fscore                          ": "0.92936",
    "generalization                  ": "0.97567",
    "metricsAverageWeight            ": "0.89977",
    "precision                       ": "0.99061",
    "simplicity                      ": "0.75758"
  },
  "uncovered_places_s_component": [
    "p1",
    "p8",
    "p7",
    "source",
    "sink",
    "p0",
    "p5",
    "p2",
    "p6",
    "p3",
    "p4"
  ]
}