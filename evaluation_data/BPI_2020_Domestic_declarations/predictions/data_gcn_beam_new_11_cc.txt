{
  "easy_soundness": false,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2020_Domestic_declarations/predictions/data_gcn_beam_new_11_cc.txt",
  "full": {
    "fitness                         ": "0.79315",
    "fscore                          ": "0.51706",
    "generalization                  ": "0.77634",
    "metricsAverageWeight            ": "0.59588",
    "precision                       ": "0.38355",
    "simplicity                      ": "0.43046"
  },
  "s_components": [],
  "sound": false,
  "top 30": {
    "fitness                         ": "0.79378",
    "fscore                          ": "0.44292",
    "generalization                  ": "0.76282",
    "metricsAverageWeight            ": "0.57355",
    "precision                       ": "0.30715",
    "simplicity                      ": "0.43046"
  },
  "uncovered_places_s_component": [
    "p21",
    "p5",
    "p22",
    "p11",
    "source",
    "p12",
    "p8",
    "p14",
    "p2",
    "p7",
    "p4",
    "p13",
    "p16",
    "p0",
    "p17",
    "p1",
    "p6",
    "p10",
    "sink",
    "p3",
    "p19",
    "p18",
    "p15",
    "p9",
    "p20"
  ]
}