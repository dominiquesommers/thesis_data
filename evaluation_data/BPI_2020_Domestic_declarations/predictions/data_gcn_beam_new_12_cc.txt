{
  "easy_soundness": false,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2020_Domestic_declarations/predictions/data_gcn_beam_new_12_cc.txt",
  "full": {
    "fitness                         ": "0.77839",
    "fscore                          ": "0.51389",
    "generalization                  ": "0.77634",
    "metricsAverageWeight            ": "0.59219",
    "precision                       ": "0.38355",
    "simplicity                      ": "0.43046"
  },
  "s_components": [],
  "sound": false,
  "top 30": {
    "fitness                         ": "0.77881",
    "fscore                          ": "0.44056",
    "generalization                  ": "0.76282",
    "metricsAverageWeight            ": "0.56981",
    "precision                       ": "0.30715",
    "simplicity                      ": "0.43046"
  },
  "uncovered_places_s_component": [
    "p14",
    "p16",
    "p12",
    "p9",
    "p13",
    "p6",
    "p21",
    "p19",
    "p3",
    "p8",
    "p10",
    "p18",
    "p7",
    "p20",
    "source",
    "p2",
    "p22",
    "p5",
    "sink",
    "p4",
    "p15",
    "p1",
    "p11",
    "p17",
    "p0"
  ]
}