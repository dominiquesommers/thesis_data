FULL
{
  "fitness": "0.743",
  "fscore": "0.827",
  "generalization": "0.943",
  "metricsAverageWeight": "0.745",
  "precision": "0.932",
  "simplicity": "0.363"
}
Top 30:
{
  "fitness": "0.746",
  "fscore": "0.829",
  "generalization": "0.850",
  "metricsAverageWeight": "0.723",
  "precision": "0.933",
  "simplicity": "0.363"
}