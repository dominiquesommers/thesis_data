FULL
{
  "fitness": "0.697",
  "fscore": "0.821",
  "generalization": "0.957",
  "metricsAverageWeight": "0.834",
  "precision": "1.000",
  "simplicity": "0.683"
}
Top 30:
{
  "fitness": "0.709",
  "fscore": "0.830",
  "generalization": "0.886",
  "metricsAverageWeight": "0.819",
  "precision": "1.000",
  "simplicity": "0.683"
}