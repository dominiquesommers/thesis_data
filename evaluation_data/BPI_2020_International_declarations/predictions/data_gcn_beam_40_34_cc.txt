FULL
{
  "fitness": "0.732",
  "fscore": "0.845",
  "generalization": "0.963",
  "metricsAverageWeight": "0.835",
  "precision": "1.000",
  "simplicity": "0.647"
}
Top 30:
{
  "fitness": "0.745",
  "fscore": "0.854",
  "generalization": "0.849",
  "metricsAverageWeight": "0.810",
  "precision": "1.000",
  "simplicity": "0.647"
}