{
  "easy_soundness": false,
  "filename": "/home/dominique/TUe/thesis/git_data/evaluation_data/BPI_2020_International_declarations/predictions/data_gcn_candidates_frequency_new_5_traces_testtesttest_2_cc.txt",
  "full": {
    "fitness                         ": "0.87471",
    "fscore                          ": "0.91262",
    "generalization                  ": "0.98036",
    "metricsAverageWeight            ": "0.90124",
    "precision                       ": "0.95396",
    "simplicity                      ": "0.79592"
  },
  "s_components": [
    [
      "Declaration SUBMITTED by EMPLOYEE",
      "p14",
      "p13",
      "Declaration REJECTED by EMPLOYEE",
      "p15",
      "Declaration REJECTED by ADMINISTRATION"
    ],
    [
      "Declaration FINAL_APPROVED by SUPERVISOR",
      "End trip",
      "Permit FINAL_APPROVED by SUPERVISOR",
      "p8",
      "p0",
      "p1",
      "p9",
      "p7",
      "Declaration SUBMITTED by EMPLOYEE",
      "Permit APPROVED by ADMINISTRATION",
      "p5",
      "p12",
      "Permit SUBMITTED by EMPLOYEE",
      "sink",
      "Declaration APPROVED by ADMINISTRATION",
      "Request Payment",
      "p11",
      "p10",
      "p2",
      "p6",
      "p4",
      "Permit APPROVED by BUDGET OWNER",
      "Start trip",
      "st2",
      "Declaration APPROVED by PRE_APPROVER",
      "st1",
      "source",
      "short_circuited_transition",
      ">",
      "Payment Handled",
      "|",
      "Declaration APPROVED by BUDGET OWNER",
      "st0",
      "Permit APPROVED by PRE_APPROVER",
      "p3"
    ]
  ],
  "sound": false,
  "top 30": {
    "fitness                         ": "0.89819",
    "fscore                          ": "0.93274",
    "generalization                  ": "0.97585",
    "metricsAverageWeight            ": "0.91001",
    "precision                       ": "0.97007",
    "simplicity                      ": "0.79592"
  },
  "uncovered_places_s_component": []
}