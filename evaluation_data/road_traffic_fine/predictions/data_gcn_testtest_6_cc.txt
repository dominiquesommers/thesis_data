FULL
{
  "fitness": "0.543",
  "fscore": "0.704",
  "generalization": "0.986",
  "metricsAverageWeight": "0.864",
  "precision": "1.000",
  "simplicity": "0.926"
}
Top 30:
{
  "fitness": "0.542",
  "fscore": "0.703",
  "generalization": "0.983",
  "metricsAverageWeight": "0.863",
  "precision": "1.000",
  "simplicity": "0.926"
}