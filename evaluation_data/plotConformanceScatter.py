import csv
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import argparse
import os
from os import listdir
from os.path import isdir, join
import string


parser = argparse.ArgumentParser()
parser.add_argument('-d', '--data_directory', help='data directory', type=str)
args = parser.parse_args()

path = '/home/dominique/TUe/thesis/git_data/evaluation_data/'

dataset_names = sorted([f for f in listdir(path) if isdir(join(path, f)) and f[0] != '.'])

X = ['fScore', 'Fitness', 'Precision', 'Simplicity', 'Generalization', 'MetricsAverageWeight', '#p', '#st']
X = ['fScore', 'Simplicity']
X = ['Fitness', 'Generalization']

datasets = {}
for dataset_name in dataset_names:
  datasets[dataset_name] = {}
  with open(f'{dataset_name}/predictions/data_conformance_best.csv', 'r') as file:
    reader = csv.reader(file)
    table = list(reader)
    data = {}
    header = [v.lower() for v in table[0]]
    X_indices = [header.index(x.lower()) for x in X]
    for row in table[1:]:
      data[row[0]] = [float(row[i]) for i in X_indices]
    data_sorted = {}
    for key in sorted(data.keys()):
      datasets[dataset_name][key] = data[key]

plt.style.use('ggplot')
fig, ax = plt.subplots()

markers = ['o', 'v', '^', '<', '>', '*', 'h', 'H', '8', 's', 'p']
markers = ['o', 'v', '1', 's', 'P', '+', 'x', '*', '|', '_', 'd']
# markers = [f'${l}$' for l in string.ascii_lowercase[:11]]
colors = ['r', 'b', 'g', 'y', 'k', 'm']
for dataset_index, (dataset_name, algorithm_conformance) in enumerate(datasets.items()):
  # if 'BPI_2020' in dataset_name:
  #   continue
  for algorithm_index, (algorithm, conformance) in enumerate(algorithm_conformance.items()):
    if 'alpha' in algorithm or 'inductive' in algorithm:
      continue
    ax.plot([conformance[1]], [conformance[0]], marker=markers[dataset_index], linestyle='', color=colors[algorithm_index], alpha=.7, ms=16)
    if dataset_index == 0:
      ax.plot([1], [1], color=colors[algorithm_index], label=f'{algorithm}')
  ax.plot([1], [1], color='k', marker=markers[dataset_index], label=f'{dataset_name}', linestyle='')
ax.set_xlabel(X[1])
ax.set_ylabel(X[0])
ax.legend()

plt.show()